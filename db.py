#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Модуль для работы с SQLite и PostgreSQL базами.
"""
import json
import sqlite3

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def define_postgre_database_url_from_file(file_name: str) -> dict or None:
    """
    Выбор параметров из файла .json для подключения/создания базы данных.
    :param file_name: Название файла с параметрами.
    В файле:
    {
        "user": "your_username",
        "password": "your_password",
        "host": "server_address",
        "port": "5433",
        "database": "your_database_name"
    }
    :return: Словарь с параметрами или None, если файл не найден или данные не json.
    """
    try:
        return json.load(open(file_name))
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        return None


def create_postgre_db(db_name: str, server_url: dict) -> psycopg2.connect:
    """
    Создаём новую PostgreSQL базу на заданном сервере.
    :param db_name: Название базы.
    :param server_url:
    {
        "user": "your_username",
        "password": "your_password",
        "host": "server_address",
        "port": "5433",
    }
    :return: None
    """
    data = {
        "user": server_url["user"],
        "password": server_url["password"],
        "host": server_url["host"],
        "port": server_url["port"],
    }
    try:
        connection = psycopg2.connect(**data)
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        query = f"CREATE DATABASE {db_name};"
        execute(dbase=connection, query=query)
        print(f'DataBase "{db_name}" created.')
        return connection
    except (Exception, psycopg2.Error) as error:
        print("Ошибка при работе с PostgreSQL: ", error)


def open_sqlite_db(db_filename: str) -> sqlite3.connect:
    """
    Создаём объект sqlite-базы.
    :param db_filename: Имя файла с базой.
    :return: Объект подключения к sqlite-базе. sqlite3.connect
    """
    db = sqlite3.connect(db_filename, check_same_thread=False)
    print(f'DBase "{db_filename}" connected.')
    return db


def open_postgre_db(url: dict = None) -> psycopg2.connect:
    """
    Подключаемся к существующей базе.
    :param url:
    {
        "user": "your_username",
        "password": "your_password",
        "host": "server_address",
        "port": "5433",
        "database": "your_database_name"
    }
    :return: объект подключения к базе. psycopg2.connect
    """
    try:
        connection = psycopg2.connect(**url)
        if connection:
            print(f'DBase "{url["database"]}" connected.')
        return connection
    except (Exception, psycopg2.Error) as error:
        print("Ошибка при работе с PostgreSQL: ", error)


def execute(dbase: psycopg2.connect or sqlite3.connect,
            query: str, return_cursor: bool = False) -> sqlite3.Cursor or psycopg2.extensions.cursor or tuple or None:
    """
    Создаёт курсор, отправляет запрос в базу, коммитит и закрывает курсор.
    Если запрос содержит 'SELECT', возвращает запрошенные данные из базы. Если return_cursor=True, возвращает курсор.
    :param return_cursor: Если True, вернёт курсор.
    :param dbase: База для работы.
    :param query: Строка с SQL-запросом.
    :return: Если запрос подразумевает получение данных из базы ('SELECT'), возвращает результат запроса.
    Если return_cursor==True, возвращает курсор. Либо просто выполняет запрос и ничего не возвращает.
    """
    handler = dbase.cursor()
    handler.execute(query)
    if return_cursor:
        return handler
    if query.lower().startswith(('select',)):
        result = handler.fetchall()
        handler.close()
        return result
    dbase.commit()
    handler.close()


def create_table(dbase: psycopg2.connect or sqlite3.connect, table_name: str, columns_to_add: str) -> None:
    """
    Создание новой таблицы в существующей базе.
    :param table_name: Название таблицы.
    :param dbase: База для работы.
    :param columns_to_add: строка с параметрами столбцов вида: "название тип_данных not null, название тип_данных"
    :return: None
    """
    query = f"CREATE TABLE IF NOT EXISTS {table_name}({columns_to_add});"
    execute(dbase=dbase, query=query)
    print(f'Table "{table_name}" created.')


def delete_table(dbase: psycopg2.connect or sqlite3.connect, table_name: str) -> None:
    """
    Удаление таблицы.
    :param dbase: База для работы.
    :param table_name: Имя таблицы.
    :return: None.
    """
    query = f'DROP TABLE {table_name} IF EXISTS;'
    execute(dbase=dbase, query=query)


def insert_row(dbase: psycopg2.connect or sqlite3.connect, table: str, params: str, values: str) -> None:
    """
    Добавляем строку в выбранную таблицу.
    :param dbase: База для работы.
    :param table: Таблица в базе, куда будем добавлять строку.
    :param params: Столбцы таблицы, которые будем заполнять.
    :param values: Значения столбцов.
    :return: None
    """
    query = f'INSERT INTO {table} ({params}) VALUES ({values});'
    execute(dbase=dbase, query=query)
    print(f'Row "{values}" inserted in "{table}"')


def delete_row(dbase: psycopg2.connect or sqlite3.connect, table: str, param: str, value: str or int) -> None:
    """
    Удаление строки из заданной таблицы.
    :param dbase: База для работы.
    :param table: Название таблицы.
    :param param: Название колонки идентификатора удаляемой строки.
    :param value: Значение колонки идентификатора удаляемой строки.
    :return: None.
    """
    handler = dbase.cursor()
    if type(dbase) == 'sqlite3.Connection':
        handler.execute('pragma foreign_keys = on')
    query = f'DELETE FROM {table} WHERE {param}={value}'
    handler.close()
    execute(dbase=dbase, query=query)
    print(f'Row {value}deleted from "{table}"')
