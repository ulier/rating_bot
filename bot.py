#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Бот для Telegram, реагирующий на слово из thanks.py в сообщениях и считающий рейтинг тех, кого поблагодарили.
"""
import sqlite3

import psycopg2
import telebot
from prettytable import from_db_cursor, MARKDOWN

import db
from thanks import THANKS

# Settings
SQLITE_DB_NAME: str = 'sqlite.db'
DATA_BASE: sqlite3.connect or psycopg2.connect = None
BASE_TABLES: list = ['users', 'chats', 'ratings']
TOKEN_FILE_NAME: str = 'token.txt'
TOKEN: str = open(TOKEN_FILE_NAME).readline()


def bot_sqlite_database_init(dbase_name: str) -> sqlite3.connect:
    """
    Внесение в базу sqlite базовых таблиц.
    :param dbase_name: Название файла создаваемой sqlite базы.
    :return: Объект подключения к базе.
    """
    dbase = db.open_sqlite_db(db_filename=dbase_name)
    create_bot_basic_tables(dbase=dbase)
    return dbase


def bot_postgre_database_init(dbase_name: str, server_url: dict) -> None:
    """
    Создание новой базы PostgreSQL на сервере и внесение в неё базовых таблиц.
    :param dbase_name: Название базы.
    :param server_url: Параметры сервера
    {
        "user": "your_username",
        "password": "your_password",
        "host": "server_address",
        "port": "5433",
    }
    :return: None.
    """
    dbase = db.create_postgre_db(dbase_name, server_url)
    create_bot_basic_tables(dbase=dbase)


def create_bot_basic_tables(dbase: psycopg2.connect or sqlite3.connect) -> None:
    """
    Создаём базовый набор таблиц в базе.
    :param dbase: База для работы.
    :return: None.
    """
    _users_table_columns = 'user_id integer NOT NULL, ' \
                           'username character varying(128), ' \
                           'PRIMARY KEY (user_id), ' \
                           'CONSTRAINT u_user_id UNIQUE (user_id)'
    _chats_table_columns = 'chat_id integer NOT NULL, ' \
                           'chat_name character varying(128), ' \
                           'PRIMARY KEY (chat_id), ' \
                           'CONSTRAINT u_user_id UNIQUE (chat_id)'
    _ratings_table_columns = 'user_id integer NOT NULL, ' \
                             'chat_id integer NOT NULL, ' \
                             'rating integer NOT NULL, ' \
                             'PRIMARY KEY (user_id, chat_id), ' \
                             'CONSTRAINT fk_user_id FOREIGN KEY (user_id) ' \
                             'REFERENCES users (user_id) MATCH SIMPLE ON DELETE CASCADE, ' \
                             'CONSTRAINT fk_chat_id FOREIGN KEY (chat_id)  ' \
                             'REFERENCES chats (chat_id) MATCH SIMPLE ON DELETE CASCADE'

    db.create_table(table_name='users', dbase=dbase, columns_to_add=_users_table_columns)
    db.create_table(table_name='chats', dbase=dbase, columns_to_add=_chats_table_columns)
    db.create_table(table_name='ratings', dbase=dbase, columns_to_add=_ratings_table_columns)


def check_bot_tables(dbase: psycopg2.connect or sqlite3.connect, list_table_names: list) -> None:
    """
    Проверяем наличие необходимых таблиц в базе,
    если какой-то таблицы нет - запускаем функцию создания таблиц.
    :param dbase: Объект базы данных.
    :param list_table_names: Список имён таблиц для проверки их наличия в базе.
    :return: None.
    """

    def check_result(query) -> None:
        result = db.execute(dbase=dbase, query=query)
        for table_name in list_table_names:
            if table_name not in result:
                create_bot_basic_tables(dbase=dbase)
                break

    # sqlite
    if type(dbase) == 'sqlite3.Connection':
        query_str = 'SELECT name FROM sqlite_master WHERE type="table"'
        check_result(query=query_str)
    # PostgreSQL
    elif type(dbase) == 'psycopg2.extensions.connection':
        query_str = 'SELECT table_name FROM information_schema.tables WHERE ' \
                    'table_schema NOT IN ("information_schema","pg_catalog");'
        check_result(query=query_str)


def add_new_chat(chat_id: int, chat_name: str, dbase: psycopg2.connect or sqlite3.connect) -> None:
    """
    Вносит новый чат в таблицу 'chats'.
    :param chat_id: chat_id.
    :param chat_name: chat_name.
    :param dbase: База для работы.
    :return: None.
    """
    table = 'chats'
    params = 'chat_id, chat_name'
    values = f'{chat_id}, {chat_name}'
    db.insert_row(dbase=dbase, table=table, params=params, values=values)


def add_new_user(user_id: int, username: str, dbase: psycopg2.connect or sqlite3.connect) -> None:
    """
    Вносит нового юзера в таблицу 'users'.
    :param user_id: chat_id.
    :param username: chat_name.
    :param dbase: База для работы.
    :return: None.
    """
    table = 'users'
    params = 'user_id, username'
    values = f'{user_id}, {username}'
    db.insert_row(dbase=dbase, table=table, params=params, values=values)


def add_new_rating(user_id: int, chat_id: int, dbase: psycopg2.connect or sqlite3.connect) -> None:
    """
    Вносит новый рейтинг в таблицу 'ratings'.
    :param user_id: user_id.
    :param chat_id: chat_id.
    :param dbase: База для работы.
    :return: None.
    """
    table = 'ratings'
    params = 'user_id, chat_id, rating'
    values = f'{user_id}, {chat_id}, 0'
    db.insert_row(dbase=dbase, table=table, params=params, values=values)


def chek_user(user: dict, dbase: sqlite3.connect or psycopg2.connect) -> int:
    """
    Есть ли юзер в БД? Если нет - добавляем.
    Есть ли чат в БД? Если нет - добавляем.
    Если есть и то и другое: смотрим и возвращаем рейтинг.
    Если чего-то нет, добавляем строку в рейтинги и ставим 0.
    :param dbase: База для работы.
    :param user: Словарь с данными пользователя и чата.
    :return: Текущий рейтинг пользователя.
    """
    user_id = int(user["user_id"])
    username = repr(user["username"])
    chat_id = int(user["chat_id"])
    chat_name = repr(user["chat_name"])

    # chat
    query = f'SELECT * FROM chats WHERE chat_id={chat_id};'
    query_result = db.execute(dbase=dbase, query=query)
    if not query_result:
        add_new_chat(chat_id=chat_id, chat_name=chat_name, dbase=dbase)

    # user
    query = f'SELECT * FROM users WHERE user_id={user_id};'
    query_result = db.execute(dbase=dbase, query=query)
    if not query_result:
        add_new_user(user_id=user_id, username=username, dbase=dbase)

    # rating
    query = f'SELECT rating FROM ratings WHERE user_id={user_id} AND chat_id={chat_id};'
    rating = db.execute(dbase=dbase, query=query)
    if not rating:
        add_new_rating(user_id=user_id, chat_id=chat_id, dbase=dbase)
        return 0
    else:
        return rating[0][0]


def change_rating(user: dict, dbase) -> int:
    """
    Запрашиваем текущий рейтинг юзера и увеличиваем его на 1.
    :param dbase: База для работы.
    :param user: Словарь с данными пользователя и чата.
    :return: Текущий рейтинг. -> int
    """
    user_rating = chek_user(user=user, dbase=dbase) + 1
    query = f'UPDATE ratings SET rating = {user_rating} ' \
            f'WHERE user_id = {user["user_id"]} ' \
            f'AND chat_id = {user["chat_id"]};'
    db.execute(dbase=dbase, query=query)
    print(f'Rating of user: "{user["user_id"]}" username: "{user["username"]}" '
          f'was changed to {user_rating}')
    return user_rating


def message_processing(bot_object: telebot.TeleBot, message: telebot.types.Message) -> dict:
    """
    Вынимаем из сообщения необходимые данные.
    :param bot_object: Объект бота.
    :param message: Обрабатываемое сообщение.
    :return: Словарь с данными. dict.
    """
    user_id = message.from_user.id
    chat_id = message.chat.id
    chat_name = message.chat.title
    user = bot_object.get_chat_member(chat_id, user_id).user
    username = f'{user.first_name} {user.last_name if user.last_name else ""}'

    current_user = {
        'user_id': user_id,
        'chat_id': chat_id,
        'chat_name': chat_name,
        'username': username,
    }

    quoted_user = {}
    if message.reply_to_message:
        quoted_msg_user_id = message.reply_to_message.from_user.id
        quoted_msg_user = bot_object.get_chat_member(chat_id, quoted_msg_user_id).user
        quoted_msg_username = f'{quoted_msg_user.first_name} ' \
                              f'{quoted_msg_user.last_name if quoted_msg_user.last_name else ""}'
        quoted_user.update({
            'user_id': quoted_msg_user_id,
            'chat_id': chat_id,
            'chat_name': chat_name,
            'username': quoted_msg_username
        })

    return {'current_user': current_user, 'quoted_user': quoted_user}


def bot_rating_processing(bot_object: telebot.TeleBot, message: telebot.types.Message) -> None:
    """
    Меняем рейтинг полезного пользователя.
    :param bot_object: Объект бота.
    :param message: Обрабатываемое сообщение.
    :return: None.
    """
    for thank in THANKS:
        if thank in message.text and message.reply_to_message:
            msg_attributes = message_processing(bot_object=bot_object, message=message)
            change_rating(user=msg_attributes['quoted_user'], dbase=DATA_BASE)


def bot_get_rating(bot_object: telebot.TeleBot, message: telebot.types.Message) -> None:
    """
    Отправляет текущий рейтинг отправившему команду '/rate'.
    :param bot_object: Объект бота.
    :param message: Обрабатываемое сообщение.
    :return: None.
    """
    msg_attributes = message_processing(bot_object=bot_object, message=message)
    rating = chek_user(user=msg_attributes['current_user'], dbase=DATA_BASE)
    bot.send_message(chat_id=msg_attributes['current_user']['chat_id'],
                     text=f"{msg_attributes['current_user']['username']}, "
                          f"твой рейтинг {rating}.",
                     reply_to_message_id=message.id)


def bot_get_all_ratings(bot_object: telebot.TeleBot, message: telebot.types.Message) -> None:
    """
    Отправит в чат таблицу рейтинга всех, у кого есть рейтинг.
    :param bot_object: Объект бота.
    :param message: Обрабатываемое сообщение.
    :return: None.
    """
    msg_attributes = message_processing(bot_object=bot_object, message=message)
    query = 'SELECT users.username, rating FROM ratings ' \
            'INNER JOIN users ON ratings.user_id=users.user_id ' \
            'INNER JOIN chats ON ratings.chat_id=chats.chat_id;'
    handler = db.execute(dbase=DATA_BASE, query=query, return_cursor=True)
    rating_table = from_db_cursor(handler)
    handler.close()
    # table style
    rating_table.set_style(MARKDOWN)
    rating_table.max_width = 15
    rating_table.left_padding_width = 0
    rating_table.right_padding_width = 0
    rating_table_output = f'<pre>{rating_table.get_string(sortby="rating", reversesort=True)}</pre>'

    bot.send_message(chat_id=msg_attributes['current_user']['chat_id'],
                     text=rating_table_output, parse_mode='HTML')


if __name__ == '__main__':
    DATA_BASE = bot_sqlite_database_init(SQLITE_DB_NAME)
    # пример подключения базы Postgre с параметрами из файла
    # DATA_BASE = db.open_postgre_db(db.define_postgre_database_url_from_file('db_params.json'))
    check_bot_tables(dbase=DATA_BASE, list_table_names=BASE_TABLES)
    bot = telebot.TeleBot(TOKEN)


    @bot.message_handler(content_types=['text'])
    def get_message(message: telebot.types.Message) -> None:
        """
        Запускает обработку сообщений из чата. Если сообщение содержит команду '/rate' передаёт
        обработчику команд 'bot_get_rating'.
        :param message: Обрабатываемое сообщение.
        :return: None.
        """
        if message.text.startswith('/rate'):
            bot_get_rating(bot_object=bot, message=message)
        elif message.text.startswith('/all_ratings'):
            bot_get_all_ratings(bot_object=bot, message=message)
        else:
            bot_rating_processing(bot_object=bot, message=message)


    @bot.edited_message_handler(content_types='text')
    def get_edited_messages(message: telebot.types.Message) -> None:
        """
        Запускает обработку изменённых сообщений из чата.
        :param message: Обрабатываемое сообщение.
        :return: None.
        """
        bot_rating_processing(bot_object=bot, message=message)


    bot.polling(none_stop=True, interval=0, long_polling_timeout=200)

# TODO: Добавить обработку команд в командной строке (либо click, либо самому написать)
# TODO: сделать единую функцию или класс для add_new
# TODO: починить /rate в общении с ботом в боте
