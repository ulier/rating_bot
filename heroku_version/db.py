#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    A module for working with a database.
"""

import psycopg2


def db_open(url) -> psycopg2.connect:
    """
    Connecting existing database.
    :param url: Connection parameters.
    :return: Database connect object. -> psycopg2.connect
    """
    try:
        connection = psycopg2.connect(url, sslmode='require')
        if connection:
            print('DBase connected.')
            return connection
    except (Exception, psycopg2.Error) as error:
        print("PostgreSQL ERROR: ", error)


def execute(dbase: psycopg2.connect, query: str, return_cursor: bool = False) -> \
        psycopg2.extensions.cursor or tuple or None:
    """
    Creates a cursor, sends a request to the database, commits and closes the cursor.
    If the request contains ('SELECT'), it returns data. If return_cursor==True it returns cursor.
    :param return_cursor: If True, returns cursor
    :param dbase: Working base.
    :param query: SQL-query string.
    :return: If the request implies getting data from the database ('SELECT'), returns
    the result of the request. If return_cursor==True it returns cursor. Or just executes the request
    and returns nothing. -> tuple or None.
    """
    handler = dbase.cursor()
    handler.execute(query)
    if return_cursor:
        return handler
    if query.lower().startswith(('select',)):
        result = handler.fetchall()
        handler.close()
        return result
    dbase.commit()
    handler.close()


def create_table(table_name: str, dbase: psycopg2.connect, columns_to_add: str) -> None:
    """
    Creating a new table in an existing database.
    :param table_name: Name of the table.
    :param dbase: Working base.
    :param columns_to_add: Row with columns parameters: "name data_type NOT NULL, name data_type"
    :return: None.
    """
    query = f"CREATE TABLE IF NOT EXISTS {table_name}({columns_to_add});"
    execute(dbase=dbase, query=query)
    print(f'Table "{table_name}" created.')


def insert_row(dbase: psycopg2.connect, table: str, params: str, values: str) -> None:
    """
    Adding a row to the selected table.
    :param dbase: Working base.
    :param table: Destination table.
    :param params: Columns for fill.
    :param values: Values.
    :return: None.
    """
    query = f'INSERT INTO {table} ({params}) VALUES ({values});'
    execute(dbase=dbase, query=query)
    print(f'Row "{values}" inserted in "{table}"')
