#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    A Telegram bot that counting the rating of those who were thanked.
"""

import os

import psycopg2
import telebot
from prettytable import from_db_cursor, MARKDOWN

import db
from thanks import THANKS

URL = os.environ['DATABASE_URL']
DATA_BASE = db.db_open(url=URL)
BASE_TABLES: list = ['users', 'chats', 'ratings']
TOKEN_FILE_NAME: str = 'token.txt'
TOKEN = open(TOKEN_FILE_NAME).readline()


def create_bot_basic_tables(dbase: psycopg2.connect) -> None:
    """
    Creating a basic set of tables in the database.
    :param dbase: Base for work.
    :return: None.
    """
    _users_table_columns = 'user_id integer NOT NULL, ' \
                           'username character varying(128), ' \
                           'PRIMARY KEY (user_id), ' \
                           'CONSTRAINT u_user_id UNIQUE (user_id)'
    _chats_table_columns = 'chat_id integer NOT NULL, ' \
                           'chat_name character varying(128), ' \
                           'PRIMARY KEY (chat_id), ' \
                           'CONSTRAINT u_user_id UNIQUE (chat_id)'
    _ratings_table_columns = 'user_id integer NOT NULL, ' \
                             'chat_id integer NOT NULL, ' \
                             'rating integer NOT NULL, ' \
                             'PRIMARY KEY (user_id, chat_id), ' \
                             'CONSTRAINT fk_user_id FOREIGN KEY (user_id) ' \
                             'REFERENCES users (user_id) MATCH SIMPLE ON DELETE CASCADE, ' \
                             'CONSTRAINT fk_chat_id FOREIGN KEY (chat_id)  ' \
                             'REFERENCES chats (chat_id) MATCH SIMPLE ON DELETE CASCADE'

    db.create_table(table_name='users', dbase=dbase, columns_to_add=_users_table_columns)
    db.create_table(table_name='chats', dbase=dbase, columns_to_add=_chats_table_columns)
    db.create_table(table_name='ratings', dbase=dbase, columns_to_add=_ratings_table_columns)


def check_bot_tables(dbase: psycopg2.connect, list_table_names: list) -> None:
    """
    Check the availability of the necessary tables in the database,
    if there is no table, run the table creation function.
    :param dbase: Working base.
    :param list_table_names: List of table names to check for their presence in the database.
    :return: None.
    """
    query = "SELECT table_name FROM information_schema.tables where table_schema='public';"
    result = db.execute(dbase=dbase, query=query)
    for table_name in list_table_names:
        if table_name not in result:
            create_bot_basic_tables(dbase=dbase)
            break


def add_new_chat(chat_id: int, chat_name: str, dbase: psycopg2.connect) -> None:
    """
   Adds a new chat to the 'chats' table.
   :param chat_id: chat_id.
   :param chat_name: chat_name.
   :param dbase: Working base.
   :return: None.
   """
    table = 'chats'
    params = 'chat_id, chat_name'
    values = f'{chat_id}, {chat_name}'
    db.insert_row(dbase=dbase, table=table, params=params, values=values)


def add_new_user(user_id: int, username: str, dbase: psycopg2.connect) -> None:
    """
    Adds a new user to the 'users' table.
    :param user_id: user_id.
    :param username: username.
    :param dbase: Working base.
    :return: None.
    """
    table = 'users'
    params = 'user_id, username'
    values = f'{user_id}, {username}'
    db.insert_row(dbase=dbase, table=table, params=params, values=values)


def add_new_rating(user_id: int, chat_id: int, dbase: psycopg2.connect) -> None:
    """
    Adds a new rating to the 'ratings' table.
    :param user_id: user_id.
    :param chat_id: username.
    :param dbase: Working base.
    :return: None.
    """
    table = 'ratings'
    params = 'user_id, chat_id, rating'
    values = f'{user_id}, {chat_id}, 0'
    db.insert_row(dbase=dbase, table=table, params=params, values=values)


def chek_user(dbase: db.psycopg2.connect, user: dict) -> int:
    """
    Is there a user in the database? If not, add it.
    Is there a chat in the database? If not, add it.
    If there is both: look and return the rating.
    If something is missing, add the line to the ratings and set it to 0.
    :param dbase: Working base.
    :param user: Dictionary with user and chat data.
    :return: Current rating of user. -> int
    """
    user_id = int(user["user_id"])
    username = repr(user["username"])
    chat_id = int(user["chat_id"])
    chat_name = repr(user["chat_name"])

    # user
    query = f'SELECT * FROM users WHERE user_id={user_id};'
    query_result = db.execute(dbase=dbase, query=query)
    if not query_result:
        add_new_user(user_id=user_id, username=username, dbase=dbase)

    # chat
    query = f'SELECT * FROM chats WHERE chat_id={chat_id};'
    query_result = db.execute(dbase=dbase, query=query)
    if not query_result:
        add_new_chat(chat_id=chat_id, chat_name=chat_name, dbase=dbase)

    # rating
    query = f'SELECT rating FROM ratings WHERE user_id={user_id} AND chat_id={chat_id};'
    rating = db.execute(dbase=dbase, query=query)
    if not rating:
        add_new_rating(user_id=int(user["user_id"]), chat_id=int(user["chat_id"]), dbase=dbase)
        return 0
    else:
        return rating[0][0]


def change_rating(dbase: db.psycopg2.connect, user: dict) -> int:
    """
    Request the current user rating and increase it by 1.
    :param dbase: Working base.
    :param user: Dictionary with user and chat data.
    :return: Current rating. -> int
    """
    user_rating = chek_user(user=user, dbase=dbase) + 1
    query = f'UPDATE ratings SET rating = {user_rating} ' \
            f'WHERE user_id = {user["user_id"]} ' \
            f'AND chat_id = {user["chat_id"]};'
    db.execute(dbase=dbase, query=query)
    print(f'Rating of user: "{user["user_id"]}" username: "{user["username"]}" '
          f'was changed to {user_rating}')
    return user_rating


def message_processing(bot_object: telebot.TeleBot, message: telebot.types.Message) -> dict:
    """
    Take the necessary data from the message.
    :param bot_object: Bot object.
    :param message: Processed message.
    :return: Dictionary with data. -> dict
    """
    user_id = str(message.from_user.id)
    chat_id = message.chat.id
    chat_name = message.chat.title
    user = bot_object.get_chat_member(chat_id, int(user_id)).user
    username = f'{user.first_name} {user.last_name if user.last_name else ""}'

    current_user = {
        'user_id': user_id,
        'chat_id': chat_id,
        'chat_name': chat_name,
        'username': username,
    }

    quoted_user = {}
    if message.reply_to_message:
        quoted_msg_user_id = message.reply_to_message.from_user.id
        quoted_msg_user = bot_object.get_chat_member(chat_id, quoted_msg_user_id).user
        quoted_msg_username = f'{quoted_msg_user.first_name} ' \
                              f'{quoted_msg_user.last_name if quoted_msg_user.last_name else ""}'
        quoted_user.update({
            'user_id': quoted_msg_user_id,
            'chat_id': chat_id,
            'chat_name': chat_name,
            'username': quoted_msg_username
        })

    return {'current_user': current_user, 'quoted_user': quoted_user}


def bot_rating_processing(bot_object: telebot.TeleBot, message: telebot.types.Message) -> None:
    """
    Changing the rating of a useful user.
    :param bot_object: Bot object.
    :param message: Processed message.
    :return: None.
    """
    for thank in THANKS:
        if thank in message.text and message.reply_to_message:
            msg_attributes = message_processing(bot_object=bot_object, message=message)
            change_rating(user=msg_attributes['quoted_user'], dbase=DATA_BASE)


def bot_get_rating(bot_object: telebot.TeleBot, message: telebot.types.Message) -> None:
    """
    Sends the current rating to the sender of the '/rate' command.
    :param bot_object: Bot object.
    :param message: Processed message.
    :return: None.
    """
    msg_attributes = message_processing(bot_object=bot_object, message=message)
    rating = chek_user(user=msg_attributes['current_user'], dbase=DATA_BASE)
    bot.send_message(chat_id=msg_attributes['current_user']['chat_id'],
                     text=f"{msg_attributes['current_user']['username']}, "
                          f"твой рейтинг {rating}.",
                     reply_to_message_id=message.id)


def bot_get_all_ratings(bot_object: telebot.TeleBot, message: telebot.types.Message) -> None:
    """
    Sends a rating table to the chat.
    :param bot_object: Bot object.
    :param message: Processed message.
    :return: None.
    """
    msg_attributes = message_processing(bot_object=bot_object, message=message)
    query = 'SELECT users.username, rating FROM ratings ' \
            'INNER JOIN users ON ratings.user_id=users.user_id ' \
            'INNER JOIN chats ON ratings.chat_id=chats.chat_id;'
    handler = db.execute(dbase=DATA_BASE, query=query, return_cursor=True)
    rating_table = from_db_cursor(handler)
    handler.close()
    # table style
    rating_table.set_style(MARKDOWN)
    rating_table.max_width = 15
    rating_table.left_padding_width = 0
    rating_table.right_padding_width = 0
    rating_table_output = f'<pre>{rating_table.get_string(sortby="rating", reversesort=True)}</pre>'

    bot.send_message(chat_id=msg_attributes['current_user']['chat_id'],
                     text=rating_table_output, parse_mode='HTML')


if __name__ == '__main__':
    bot = telebot.TeleBot(TOKEN)
    check_bot_tables(dbase=DATA_BASE, list_table_names=BASE_TABLES)


    @bot.message_handler(content_types=['text'])
    def get_message(message: telebot.types.Message) -> None:
        """
        Starts processing messages from the chat. If the message contains the command '/rate ' passes
        to the 'bot_get_rating' command handler. If '/all_rating' - 'bot_get_all_ratings'.
        Else: just changes the rating if necessary.
        :param message: Processed message.
        :return: None.
        """
        if message.text.startswith('/rate'):
            bot_get_rating(bot_object=bot, message=message)
        elif message.text.startswith('/all_ratings'):
            bot_get_all_ratings(bot_object=bot, message=message)
        else:
            bot_rating_processing(bot_object=bot, message=message)


    @bot.edited_message_handler()
    def get_edited_messages(message: telebot.types.Message) -> None:
        """
        Starts processing modified messages from the chat.
        :param message: Processed message.
        :return: None.
        """
        bot_rating_processing(bot_object=bot, message=message)


    bot.polling(none_stop=True, interval=0, long_polling_timeout=200)
