# rating-bot heroku version

---

**Подготовленная версия для размещения на хостинге [heroku.com](https://www.heroku.com/ "heroku.com")**

---
## Важно!

* `heroku_version` это отдельный от основного, полностью 
готовый к использованию, вариант `rating_bot`. Эта версия 
  проверена и работает при условии 
  размещения его именно на
  [heroku.com](https://www.heroku.com/ "heroku.com"). 
  Как он поведёт себя на другом хостинге я не знаю.

* Эта версия работает только с `PostgreSQL`!
* Этой версии, как и основной, для работы требуется токен-ключ, 
который выдаёт telegram-bot `@BotFather`. Инструкция находится 
  ниже.

* Если хочется изменить название основного файла 
`rating_bot_heroku.py` на, к примеру, 
`my_super_bot.py`, в файле `Procfile` необходимо 
изменить строку  
  `worker: python3 rating_bot_heroku.py` 
  
    на 

    `worker: python3 my_super_bot.py`

* Если вдруг захочется переписать бота на версии Python 
  критично отличающейся от `3.9.6`: необходимо в файле 
  `runtime.txt` изменить версию на ту, которая будет 
  использоваться.
  
---

### Про токен.

Чтобы запустить бот, необходимо его зарегистрировать в
`Telegram`, используя телеграм-бота `@BotFather`,
в процессе общения с которым создаётся оболочка для
нового телеграм-бота и выдаётся ключ-токен
вида: `1234567890:AAHfiqksKZ8WmR2zSjiQ7_v4TMAKdiHm9T0`,
который нужен для работы с `Telegram`. Token можно
как сохранить в файле 'token.txt', так и просто
присвоить переменной TOKEN в основном файле бота 
`rating_bot_heroku.py`.
Для того, чтобы бот начал обрабатывать сообщения в
чате, в `@BotFather` необходимо включить опцию
`/setprivacy`.

```python
TOKEN = '1234567890:AAHfiqksKZ8WmR2zSjiQ7_v4TMAKdiHm9T0' # пример токена
# либо, если токен находится в файле:
TOKEN_FILE_NAME: str = 'token.txt'
```

В текущей версии проекта используется файл
`'token.txt'`, которого нет в проекте.
Его нужно создать с любым названием,
внести в него полученный токен и присвоить
переменной `TOKEN_FILE_NAME` имя файла вместо
`'token.txt'` если оно отличается.

**Без токена бот работать не будет!!!**

---

## Инструкция.

Чтобы бот работал постоянно, его нужно разместить на 
каком-либо хостинге. Здесь представлена версия бота, 
специально подготовленная для размещения на хостинге
[heroku.com](https://www.heroku.com/ "heroku.com"). 
Сервис выбран из-за его относительной бесплатности 
и достаточной надёжности.

Для того, чтобы разместить бота, необходимо сделать 
следующее:

### 1. Регистрация и размещение бота на [heroku.com](https://www.heroku.com/ "heroku.com")

Для начала, необходимо зарегистрироваться на 
[heroku.com](https://www.heroku.com/ "heroku.com").

![img.png](img/img1.png)

После регистрации жмём где удобнее на кнопку 
`Create new app`

![img.png](img/img2.png)

Вписываем название бота (произвольное, как хочется 
назвать) в строку `app-name` (без пробелов, подчёркиваний 
и заглавных букв), выбираем Европу и жмём `Create app`.

![img.png](img/img3.png)

Попадаем на страничку `Deploy`. Здесь выбираем 
способ, которым будем заливать нашего бота. Я 
заливал через `Heroku Git`, поэтому и расскажу про него.

![img.png](img/img4.png)

Инструкция от 
[heroku.com](https://www.heroku.com/ "heroku.com"):

![img.png](img/img5.png)

Для начала нужно скачать и установить
Heroku Command Line Interface 
[heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

![img.png](img/img6.png)

Далее, в командной строке нужно выполнить:
```commandline
heroku login
```

![img.png](img/img7.png)

и нажать любую клавишу. Откроется страница 
в браузере.

![img.png](img/img8.png)

Жмём на `Log in`.

Далее, в командной строке делаем следующее. 

Выбираем каталог, в котором лежит бот:
```commandline
cd MySuperBotDirectory
```
Инициализируем в нём репозиторий:
```commandline
git init
```
Сообщаем `heroku` что именно этот репозиторий 
содержит то, что мы хотим разместить 
на сервере:
```commandline
heroku git:remote -a vasyapupkinbot
```
Добавляем все файлы из каталога, в котором 
лежит бот, в репозиторий:
```commandline
git add .
```
Коммитим всё:
```commandline
git commit -am "Let's my SuperBot fly!!!"
```
И отправляем бота на сервер:
```commandline
git push heroku master
```

Всё! Бот на сервере. Можно запускать? НЕТ!
Прежде, чем запускать сам бот, необходимо создать базу данных.

---
### 2. База данных

Полная инструкция по организации базы данных на 
[heroku.com](https://www.heroku.com/ "heroku.com") 
находится тут: 
[Heroku Postgres](https://devcenter.heroku.com/articles/heroku-postgresql "Heroku Postgres")

Вот один из способов.
На главной странице 
[https://dashboard.heroku.com/apps](https://dashboard.heroku.com/apps)
в правом углу, левее аватара, нажимаем на плитку с точками:

![img.png](img/img9.png)

Выбираем `Data` и попадаем на страницу с кнопками:

![img.png](img/img10.png)

Нас интересует `Heroku Postgres`. Нажимаем `Create one` и 
переходим на страницу `Heroku Postgres`:

![img.png](img/img11.png)

Нас интересует кнопка `Install Heroku Postgres` справа. 
Нажимаем и переходим на страницу `Online Order Form`, на 
которой нужно в поле `App to provision to` ввести название 
бота, который вводили при создании:

![img.png](img/img12.png)

Жмём `Submit Order Form`.
Всё. База создана, теперь можно запускать бота.

---

### 3. Запуск бота на хостинге [heroku.com](https://www.heroku.com/ "heroku.com"):

Для запуска, необходимо перейти на вкладку `Dashboard` (нажать 
на плитку в правом углу, левее аватара):

![img.png](img/img13.png)

На открывшейся странице нажать на название бота. Если все сделали 
нормально, окажемся здесь:

![img.png](img/img14.png)

Нас интересует вкладка `Resources`. Жмём на 
карандашик справа:

![img.png](img/img15.png)



![img.png](img/img16.png)

И переключаем ползунок:

![img.png](img/img17.png)

После этого жмём `Confirm`

**Все! Вы великолепны!**

Если всё сделано правильно: бот запущен. Проверить это можно 
зайдя в лог, нажав на кнопку `More`:

![img.png](img/img18.png)


